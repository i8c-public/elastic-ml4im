# ML4IM Development Desktop
The Development Desktop is a VM you can create locally on your laptop to manage the AWS VM. It comes pre-installed with all the required tools. The Vagrant file can be found at `ml4im_dev_desktop/Vagrantfile`.
The following software is installed on a local virtual machine using Vagrant:
* Google Chrome
* Git, Java Development Kit and Maven
* Docker and Docker Machine
* Amazon Web Service Command Line Interface
* Oracle SQL Developer (with pre-configured JDK)

# Requirements
Before the installation you must download the following files:
- [Oracle SQL Developer .RPM package](https://download.oracle.com/otn/java/sqldeveloper/sqldeveloper-19.4.0.354.1759-19.4.0-354.1759.noarch.rpm): Place the .RPM package inside `/ml4im_dev_desktop`.
- [Oracle JDBC Driver 8](https://www.oracle.com/database/technologies/jdbc-ucp-122-downloads.html#license-lightbox): Download `ojdbc8.jar` and place the .JAR file inside `/ml4im_docker/install/docker-compose/logstash_ojdbc8`.
  
The downloads require you login using an Oracle account. **The required SQL Developer version is 19.4.0.**  
The RPM package is used to install Oracle SQL Developer. The JDBC driver is used later in the AWS Virtual machine to run Logstash. 
  
## Compatibility matrix
You should always try to use the latest CentOS 7 box version available and also the latest version of all the pre-requisite software components used in this project as they will include features and fixes to support the latest CentOS build.  
Below table shows the compatible versions for the software used by this project that have been tested, from new to old:  
  
| O.S. | Virtual Box | Vagrant | vbguest plugin | Centos box |
| --- | --- | --- | --- | --- | 
| Windows 10 Professional 1909 18363.476 | 6.0.14 | 2.2.7 | 0.20.0 | 7 v1905.1 |  
  
# Usage
## Prerequisites
Make sure you have installed the following utilities before continuing:  
1.  [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2.  [Vagrant](https://www.vagrantup.com/intro/getting-started/index.html)
3.  [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4.  Bash: this shell is standard available on Linux and MacOS. On Windows it should automatically be installed together with Git for Windows
  
These utilities are free and available on all major operating systems. Check the previous section for more info on which versions of the utilities that are compatible and you should install. They provides an easily reproducible and portable environment that can be used to build, maintain and run this project. Once these tools are installed, proceed as follows:
  
## Installation
1.  In your command line box, execute the following command:   
    
    ```sh
    vagrant up &>> vagrant.log &
    tail -f vagrant.log
    ```
    
    Don't login to your machine as long as it only shows a command line terminal. Wait for it to finish its installation of the graphical shell. This can take some time. You can check the progress on the command line of your laptop.
    
2.  Login to your machine

    Once your the graphical shell in your VM is visible you can login using the user "vagrant" with password "vagrant". Note that the keyboard layout has been changed by Vagrant to Belgian azerty when you enter the password. If this would not be correct, you will be able to change the keyboard layout upon first login. Once you have logged in, you can also change the password by clicking the icons in the upper right corner of the screen, click the tools icon and select 'Region & Language' -> 'Input Sources'. In the windows that appears click on the "+" sign to add your keyboard as a new input source. After adding your input source, you can remove incorrect input sources. 
      
    Mind that some software may still be installing after you can use the graphical shell. It is advised you wait until the installation is complete before continuing. After the installation is complete the development desktop will restart and you may create the [ML4IM AWS Virtual Machine](./../ml4im_docker/README.md).  
