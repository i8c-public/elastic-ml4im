#!/bin/bash
# Setup script to prepare the AWS Virtual Machine.
echo "### Starting setup ###"
echo "### Transferring files to AWS VM.. ###"
docker-machine ssh ML4IMDocker mkdir -m 777 /home/ubuntu/ml4imDocker
docker-machine scp -r /vagrant/ml4im_docker ML4IMDocker:/home/ubuntu/ml4imDocker
docker-machine ssh ML4IMDocker sudo chown -R ubuntu /home/ubuntu/ml4imDocker
echo "### Installing docker-compose on AWS VM.. ### "
docker-machine ssh ML4IMDocker sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
docker-machine ssh ML4IMDocker sudo chmod +x /usr/local/bin/docker-compose
echo "### Setup completed ###"