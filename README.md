# Machine Learning for Integration Monitoring    
## Introduction
Integr8 Consulting (i8c) builds and supports integration solutions, using Hybrid Integration Platform (HIP) software. An important feature of such a platform, is a monitoring solution, existing out of a framework and dashboard, that allows integration owners to follow up the status of the integration flows running over the HIP.  
  
This project was the result of an internship, performed by Sacha Kempeneer in the spring of 2020.  
  
## Goal
This ML4IM project demonstrates how Machine Learning can be an addition to an existing integration platform monitoring solution. It provides the means to collect, store and visualize logging data, and perform machine learning on that data, using the Elastic stack. By executing machine learning jobs against logging data, it may be possible to find anomalies in processes that would be invisible to the naked eye.  
  
This project provides a lab environment to test machine learning methods. It comes with a pre-configured environment, anonymised demonstration data and example machine learning jobs. ML4IM uses a virtual machine on Amazon Web Services, that runs the Elastic Stack and an Oracle Database to store the example logging data, all hosted as docker containers.  

## Data Stream
![alt text](./assets/ml4im_platform.PNG)  

**Generic Monitoring Platform**  
*This section serves as an example on how data can be collected by a monitoring platform. It's based on an existing monitoring platform of one of i8c's customers. This part is out of scope of the ML4IM project, which already starts from sample data in the Oracle Database, which contains an anonymized export from a production monitoring system.*  

1. A monitoring platform can be used to monitor the state of interface instances. Log events are generated when an interface or process goes through multiple states, and these are collected. These log events may be any of the following:   
    - Interface starts
    - Interface stops
    - Proceeding to a next step or state
    - Unexpected errors or warnings  
  
    A preview of such logs can be seen under the *[Sample Data](#sample-data)*.
2. Collected log events are stored in the ML4IM (Oracle) Logging Database.   
  
**Machine Learning for Integration Monitoring**  
*This section provides an insight into the workings of ML4IM.*  

3. Monitoring data is held in the LOGGING_EVENTS & INTERFACE_INSTANCES tables. The latter table contains an aggregated view of the LOGGING_EVENTS table. This project comes with a data dump that populates both LOGGING_EVENTS & INTERFACE_INSTANCES tables with sample data. The SQL scripts that were used to anonymize the source data of the data dump are included in the project, only as information (located in ml4im_docker/install/docker-compose/config/oracle/setup).  
  
4. Logstash executes an SQL statement against both LOGGING_EVENTS and INTERFACE_INSTANCES. It collects the data and transforms it into 2 Elasticsearch indexes. Logstash not only transports the data to Elasticsearch but also transforms it for ease of use. Some data types are changed and a time_passed field is added to the INTERFACE_INSTANCES index. This field is calculated from the difference between start_timestamp and stop_timestamp and represents the duration of an interface instance in milliseconds.
  
5. Elasticsearch is the heart of the ML4IM solution. The data is kept here and can be used in all Elasticsearch provided functions. The INTERFACE_INSTANCES is run against pre-configured machine learning jobs. The machine learning jobs are configured with the following specifications:

    - **ml_time_variance_by_type**: The goal is to find anomalies in the time passed between the start and end of an interface instance.
      
    - **ml_completed_time_variance_by_type**: The goal is to find anomalies in the time passed between the start and end of a succesful & completed interface.

    - **ml_count_events_by_type**: The goal is to find anomalies in the high count of starting interfaces in static time-frames.

6. Kibana is used as a user interface. Here we may see the anomalies that Elasticsearch detected within our INTERFACE_INSTANCES index. Using the anomaly explorer we can find at what times these anomalies took place and what they reported. We can look further using the correlation id of that instance to find the specific logs that caused the anomaly. This gives us the opportunity to identify which systems or processes may have issues that could influence the goal of the given system.
  
## Machine Learning Demonstration  
ML4im provides an example for machine learning methods using logging data. Using machine learning jobs we were able to find anomalies in data where certain processes took way longer than expected. The following jobs are configured in the demonstration:

- **ml_time_variance_by_type**: The goal is to find anomalies in the time passed between the start and end of an interface instance. A function is executed against the index that checks the variances in time_passed, comparing it to the temporal history of previous instances with the given type.  

- **ml_time_variance_completed_by_type**: The goal is to find anomalies in the time passed between the start and end of a completed & succesful interface instance. A function is executed against the index that checks the variances in time_passed, comparing it to the temporal history of previous instances with the given type. Only interfaces with status *interface_ready* are analyzed.  

- **ml_count_events_by_type**: The goal is to find anomalies in the high count of starting interfaces in static time-frames. A function is executed against the index that checks the amount of events within a certain time-frame, comparing it to the temporal history of previous time-frames with the given type.  
  
### Anomaly Timeline
  
![](./assets/ml4im_elastic_machinelearningExampleTypes.PNG)
*The picture above shows the anomaly explorer for the "ml_time_variance_by_type" machine learning job. Here we can find useful information that Elasticsearch found in our data.*  
  
The anomaly timeline gives a visual representation on anomalies within our data. The main timeline is a combination of all types of processes and shows overall major anomalies. Below it is a breakdown of the anomaly data for each type of process in our logging data. Due to our configuration of the machine learning job, anomaly data can be viewed by type, hostname or status. This gives us the opportunity to find if certain hosts contain more anomalies than others for example.  
  
### Influencers
To the left of the timelines you can find the influencers of the machine learning job. Influencers were configured that we found may influence anomalies within the logging data. Elasticsearch gives us a visual representation on what influencers have the most anomalies and what you should watch out for.  
  
### Anomaly List  
  
![](./assets/ml4im_elastic_machinelearningExampleWarnings.PNG)
*The anomaly list used to break down time-buckets in the anomaly timeline.*  
When clicking on a time-bucket within our anomaly timeline, Elasticsearch will filter the anomaly list below to that time-bucket. This shows individual anomalies found at that time and their general information. By opening an anomaly we can find more specific information. This allows us to use Kibana to find the events in our LOGGING_EVENTS index and further research on what exactly happend and more importantly, how to prevent it from happening again.  

![](./assets/ml4im_elastic_machinelearningExplorer.PNG)  
*The originating logs found in the documents explorer that caused the anomalies*   
  
### Time Series
  
![](./assets/ml4im_elastic_machinelearningExampleTypes2.PNG)
*A time-series with detected anomalies from the "ml_count_events_by_type" machine learning job.*  
  
If a detector within a temporal machine learning job makes use of value datatypes, it has the feature to visualize anomalies in a time-series. This is the case for our second machine learning job as it deals with the number of incoming events within a certain time-frame. Visualization can be split into multiple series as seen above, or filtered on the given type.  
  
### Dealing with downtime
  
![](./assets/ml4im_elastic_machinelearningDowntime.PNG)
  
Systems often have to deal with planned downtime on certain parts of the process. As these false negatives may influence machine learning jobs there is a function within elastic that allows us to prevent this. The calendar management systems may be used to add times of planned outages that machine learning jobs will ignore in their analysis. This makes sure that only real anomalies are detected. Within our demonstration data we have provided a pre-configured calendar that deals with planned downtime within the demonstration data.
  
## Solution Architecture
The main component exist out of an AWS virtual machine that runs 4 docker containers. These docker containers contain the Elastic stack and an Oracle Database.
  
- **Oracle Database**: This database is used to hold sample data to demonstrate the solution. A script is provided that may be used to import the INTERFACE_INSTANCES & LOGGING_EVENTS table.

- **Logstash**: Logstash is used to transport and transform the data coming from the Oracle Database into Elastic Search. Using the provided configuration, it will create 2 indexes inside Elastic Search. More information can be found in the sample data section.

- **Elastic Search**: The heart of the Elastic Stack, Elastic Search provides the main components required for the solution. Elastic Search is used to process the data into useful information and execute Machine Learning methods.  

- **Kibana**: Kibana provides an interface to interact with Elastic Search. This can be used to search and analyze information, as well as visualisation of the sample data.  

This project provides a custom Vagrantfile that can be used to install a Development Desktop that includes all software required to setup the ML4IM AWS Virtual Machine and interact with it.
  
![alt text](./assets/ml4im_overview.PNG)  

## Sample data  
The project contains sample data that is used by machine learning methods. A presentation of this data be seen below. 
   
### Scripts  
- `ML4IM_DATA.DMP`: This data dump has been created using Oracle Datapump. It holds the LOGGING_EVENTS & INTERFACE_INSTANCES table.  
- `interface_instances_transform.sql`: This script is used to transform the LOGGING_EVENTS table into the INTERFACE_INSTANCES table. It is included in the project to give a better understanding about the relation between the tables.  
- `logging_events_anon_procedure.sql`: This script holds a PL/SQL stored procedure that was used to anonymize the original logging data. 
  
### Oracle Database - Log Events
The Oracle Database contains anonymized logging events from an external source. These are held in the LOGGING_EVENTS table and extracted from `ML4IM_DATA.DMP` using Oracle Datapump. The data has been anonymized by the included `logging_events_anon_procedure.sql` script.

| LOGID                            | CORRELATIONID                    | TYPE          | SERVICE      | STATUS          | MESSAGE                      | CUSTOM_1     | CUSTOM_2           | CUSTOM_3            | HOSTNAME     | USERNAME     |
|----------------------------------|----------------------------------|---------------|--------------|-----------------|------------------------------|--------------|--------------------|---------------------|--------------|--------------|
| number                           | number                           | varchar2(100) | varchar(100) | varchar(20)     | varchar(500)                 | varchar(100) | varchar(100)       | varchar(100)        | varchar(100) | varchar(100) |
| 20200317054642418152162733352503 | 20200317054642145434195210797011 | Type 1        | Service 1    | interface_start | Hidden details for service 1 | System 1     | Hidden Document ID | Hidden Interface ID | Host 1       | User 1       |  
  
### Oracle Database - Interface Instances
A table in which the logging data has been compressed in a single row with its last known state and timestamps. The INTERFACE_INSTANCES table is extracted from `ML4IM_DATA.DMP` using Oracle Datapump. It was originally created by running the included `interface_instances_transform.sql` script against the LOGGING_EVENTS table. 

| CORRELATIONID                    | TYPE          | START_TIMESTAMP | STOP_TIMESTAMP | SERVICE      | STATUS          | MESSAGE                      | CUSTOM_1     | CUSTOM_2           | HOSTNAME     | USERNAME     |
|----------------------------------|---------------|-----------------|----------------|--------------|-----------------|------------------------------|--------------|--------------------|--------------|--------------|
| number                           | varchar2(100) | TIMESTAMP(9)    | TIMESTAMP(9)   | varchar(100) | varchar(20)     | varchar(500)                 | varchar(100) | varchar(100)       | varchar(100) | varchar(100) |
| 20200317054642145434195210797011 | Type 1        | 17-MAR-20       | 17-MAR-20      | Service 1    | interface_ready | Hidden details for service 1 | System 1     | Hidden Document ID | Host 1       | User 1       |  
  
### Elastic Search - Log Events
This index is created by Logstash. It contains the logging events, transformed for ease of use.  

| logid                            | correlationid                    | message                      | timestamp                   | status          | service   | type    | custom1  | custom2            | custom3             | hostname | username |
|----------------------------------|----------------------------------|------------------------------|-----------------------------|-----------------|-----------|---------|----------|--------------------|---------------------|----------|----------|
| keyword                          | keyword                          | text                         | date                        | keyword         | keyword   | keyword | text     | text               | text                | keyword  | keyword  |
| 20200317054642418152162733352503 | 20200317054642145434195210797011 | Hidden details for service 1 | Feb 26, 2020 @ 11:06:51.330 | interface_start | Service 1 | Type 1  | System 1 | Hidden Document ID | Hidden Interface ID | Host 1   | User 1   |  

### Elastic Search - Interface Instances
This index is created by Logstash. It contains a document for each interface_instance_id. This document is the latest status of the interface, the latest log. The field time_passed also reflects how long an interface has been active (ms). 
 
| correlationid                    | message                      | start_timestamp             | end_timestamp               | time_passed | status          | type    | custom1  | custom2            | hostname | username |
|----------------------------------|------------------------------|-----------------------------|-----------------------------|-------------|-----------------|---------|----------|--------------------|----------|----------|
| keyword                          | text                         | date                        | date                        | integer     | keyword         | keyword | text     | text               | keyword  | keyword  |
| 20200317054642418152162733352503 | Hidden details for service 1 | Feb 26, 2020 @ 11:06:51.330 | Feb 26, 2020 @ 11:06:51.881 | 551         | interface_ready | Type 1  | System 1 | Hidden Document ID | Host 1   | User 1   |

## Build & Test
You can use the following instructions to build & test this solution. While the Development desktop is not required, it is used to create the ML4IM AWS Virtual Machine in an efficient manner. If you wish to skip this installation, make sure you have the required software installed to continue the setup. You can find the tools required in the ML4IM Development Desktop readme.  

- [ML4IM Development Desktop](./ml4im_dev_desktop/README.md)
- [ML4IM AWS Virtual Machine](./ml4im_docker/README.md)  

Instructions on how to prepare and use solution can be found in the [usage](./usage/README.md) section.  
  
## Troubleshooting  
You can find more information on solving potential issues in the [troubleshooting](./usage/troubleshooting.md) section.
