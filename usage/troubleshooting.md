# Troubleshooting
  
[[_TOC_]]

## Development Desktop  
### VirtualBox Guest Additions installation fails
The CentOS vagrant box used in this project is officialy supported by CentOS and frequently updated on the vagrant cloud. Vagrant downloads and uses the latest CentOS box version when you create a first instance of a VM based on that box. You can check which is the latest available version on the [Vagrant cloud](https://app.vagrantup.com/centos). The Vagrantfile also uses the Vagrant [vbguest plugin](https://github.com/dotless-de/vagrant-vbguest) to automatically install the [Virtual Box Guest Additions (VBGA)](https://www.virtualbox.org/manual/ch04.html) that match the version of your VirtualBox installation. The vbguest plugin tries to install the latest kernel header files to install VBGA on your CentOS VM. If you would run into issues when installing VBGA or related functionality, these may have different causes and resolutions:  
  
### Incompatible software versions
Make sure you are using the latest version of all components involved:
- [Vagrant](https://www.vagrantup.com/downloads.html)   
- Vagrant vbguest plugin (please note that Vagrant plugins have to be updated seperately! They are not automatically upgraded when you install the latest Vagrant version. Run `vagrant plugin update` to update all your plugins.)  
- VirtualBox (install the [latest stable release](https://www.virtualbox.org/wiki/Downloads) first, try to install a [test build](https://www.virtualbox.org/wiki/Testbuilds) to get the latest fixes in case your problem is not fixed)  
- CentOS box (run `vagrant box update` from the directory where your Vagrantfile is located to get the latest box release. Note that you have to recreate your VM to apply a new box version.)  
  
There is a large community behind each of these components. Updates are released frequently and when incompatibility issues occur between components, updates are released typically within a couple of days. Check the official websites of each component to make sure you are running the latest versions. Download and install the latest release to check if your issue is solved with the latest version of the software. You may need to install test builds to get the latest fixes for certain products.  
  
### CentOS box binaries and yum repository source files version mismatch  

If you still fail to install the VirtualBox Guest Additions after updating all components to the latest version, check your `vagrant.log` for the following errors:  
  
```sh
VirtualBox Guest Additions: Building the VirtualBox Guest Additions kernel modules.
This system is currently not set up to build kernel modules.
Please install the Linux kernel "header" files matching the current kernel
for adding new hardware support to the system.
The distribution packages containing the headers are probably:
    kernel-devel kernel-devel-3.10.0-693.21.1.el7.x86_64
An error occurred during installation of VirtualBox Guest Additions 5.2.12. Some functionality may not work as intended.
```
  
This error will probably be preceded by:
  
```sh
Loaded plugins: fastestmirror
Determining fastest mirrors
 * base: xxx
 * extras: xxx
 * updates: xxx
No package kernel-devel-3.10.0-693.21.1.el7.x86_64 available.
```
  
This problem seems to be caused by the yum repos (base, extras, updates), whose major version symlink (7 in the example that follows) already points to a higher minor version while your box expects a lower minor version. For example http://mirror.centos.org/centos/7/ can point to http://mirror.centos.org/centos/7.5.1804/ while your box expects to find the content of http://mirror.centos.org/centos/7.4.1708/. This typically occures when the latest Centos Vagrant box is not yet updated to the latest Centos binaries with a higher version.
  
To fix this issue, connect to your vagrant instance with ssh and update your environment manually:
  
  
```sh
vagrant ssh
[vagrant@localhost ~]$ sudo yum -y update
[vagrant@localhost ~]$ exit
vagrant reload
```
  
Reload your VM to make sure the kernel is upgraded. You can check this using the uname command:
  
```sh
# Before reload
[vagrant@localhost ~]$ uname -a
Linux localhost.localdomain 3.10.0-693.21.1.el7.x86_64 #1 SMP Wed Mar 7 19:03:37 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
```
```sh
# After reload
[vagrant@localhost ~]$ uname -a
Linux localhost.localdomain 3.10.0-862.2.3.el7.x86_64 #1 SMP Wed May 9 18:05:47 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
```
  
### Copy/Paste from guest to host or vice versa via clipboard doesn't work
First of all note that the copy/paste feature between your host and the CentOS guest running in VirtualBox only works for plain text.  
If this feature is not working, try the following from the command line of your host where the Vagrantfile is located to enable this feature:  
```sh
vagrant vbguest --do install
vagrant reload
```  
This will reinstall the VirtualBox Guest Additions. Make sure your desktop software and the latest updates have been installed, so that the VirtualBox Guest Additions are compiled and linked correctly.  
  
### Unresponsive mouse pointer
Logout and login to the Virtual Box UI to fix the problem with the unresponsive mouse pointer.  
  
### Additional troubleshooting tips  
  
In case your issue is still not solved, check the vbguest installation log files on the VM in the /var/log folder. Lookup the details of the error and ask your best friend Google for help.  
  
## AWS Virtual Machine    
### Docker-machine: SSH fails to connect to the virtual machine.
Possible causes:
1. `/home/vagrant/.ssh` (and included files) have the wrong permissions:
    - The .ssh directory permissions should be 700 (drwx------).
    - The public key (.pub file) should be 644 (-rw-r--r--).
    - The private key (id_rsa) on the client host and the authorized_keys file on the server should be 600 (-rw-------).  
2. Invalid AMI:
    - After testing it seems that some AMI's (tested using a Red Hat AMI) have a bug that blocks ssh from being correctly used. It is advised you change to a different AMI or use the default Ubuntu.
    

### Elastic: ElasticsearchException[Failure running machine learning native code.]
This error may show if you are trying to run Elastic Search on a processor that does not support SSE4.2 instructions. 
The only solution is to change to another machine with a processor that supports this.

  
### SQL Developer performance issues / SQL Developer not saving changes to database
This can be solved by assigning more memory to SQL Developer. You can modify memory usage in SQL developer by adding the following line to `~/.sqldeveloper/19.4.0/product.conf`:
```shell
# The following will assign 8GB of working memory:
AddVMOption -Xmx8g
# You can also use AddVMOption -Xmx8000m (= 8000MB)
```
  
### SQL Developer won't start
This is probably caused due to SQL Developer not being able to find the Java JDK. This should be automatically configured using Vagrant, if not check `~/.sqldeveloper/19.4.0/product.conf`.
```shell
# The following line sets the path to the Java JDK. The path shown below is the default location set by Vagrant and should be correct.
SetJavaHome /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.242.b08-0.el7_7.x86_64  
```  
  
### Oracle Docker container takes up large amount of disk space  
After using the database for some time you may notice that the oracle container is starting to take up large amounts of space. The most probable cause is that the temporary tablespace file inside the container is getting too large. This contains all queries and results executed on the database. If you do not require these files as backup, you may remove and recreate the file using the following commands:
  
```sql
-- The default temporary .dbf file is temp01. Make sure that this is the file causing problems by checking its size.
-- Make sure there are no other active connections while executing the commands.
alter database tempfile '/opt/oracle/oradata/XE/temp01.dbf' drop including datafiles;
create temporary tablespace temp_new tempfile '/opt/oracle/oradata/XE/temp01.dbf' size 500m autoextend on next 250m maxsize 5096m;
commit; 
```
  
This will delete and recreate the temporary tablespace file for Oracle XE and free disk space. A full disk space may have caused Elastic Search to prevent further data from being stored, it is advised you check if this is the case in the section below.

#### Elasticsearch: [cluster_block_exception] index [.security-7] blocked by: [FORBIDDEN/12/index read-only / allow delete (api)]
This happens when your VM runs out of storage space. Kibana will auto change the config to prevent further data from being stored.
1. [Use AWS to set more storage space to your system](#Expanding-disk-size-on-Amazon-Web-Services) or free disk space by removing unnecessary files.
  
2. Go to the Kibana Dev Tools section and execute the following code:  
  
```
	PUT _settings
    	{
    		"index": {
    		"blocks": {
    		"read_only_allow_delete": "false"
    		}
    		}
    	}
```

```
	PUT [Enter your index name here]/_settings
    	{
    		"index": {
    		"blocks": {
    		"read_only_allow_delete": "false"
    		}
    		}
    	}
```  
  
### Expanding disk size on Amazon Web Services  
When working with the Oracle Database or Elasticsearch, you may find yourself in a sitation with a full disk. This can easily be solved due to the fact that ML4IM uses Elastic Block Storage. The storage can be modified in the [EC2 Management Console](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Volumes:sort=desc:createTime).  
  
In the sidebar, select "volumes" under "Elastic Block Store". Here you will find a list of volumes used by your EC2 instances. Right-click on the volume that is used by the OselsDocker (volume may have an empty name) instance and select "Modify volume". You can increase the storage space for you virtual machine in this window.  
![](./../assets/ebs.PNG)  
  
After this modification has been completed you must also expand the partition on the AWS Server. Follow [this guide provided by Amazon](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/recognize-expanded-volume-linux.html) to expand the partition.  

### SQLPlus: TNS - Lost Contact when trying to login locally
This is solved by changing the permissions inside the oracle container:
```shell
# Open a bash shell inside the container (sudo docker ps to find the container ID)
$ sudo docker exec -it [Container ID] /bin/sh
# Goto the Oracle Variable location
sh-4.2# cd $ORACLE_HOME/bin
# Change the permissions using chmod
sh-4.2# chmod 6751 oracle
``` 

### Changing credentails for Kibana built-in users  
First we need to find the container id, enter the container and configure it.
```shell
# This command shows all running docker-containers. Find the container ID for "docker-compose_elasticsearch_1. (Example: '98b06824bf78')
$ sudo docker ps
# Get a bash shell for this container. Any command written in this bash executes inside the elasticsearch container!
# Example: $ sudo docker exec -it 98b06824bf78 /bin/sh
$ sudo docker exec -it [Container ID] /bin/sh
```  
Now we have a bash shell inside the Elastic Search docker-container. We can configure the passwords for each built-in user using an interactive shell.  
You will be prompted to enter (and confirm) a password for each built-in user. **You must set the password for the **kibana** user the same as you did in the `kibana.yml` file! (default: ml4im)**  
```shell
# This command will start the interactive shell:
sh-4.2# /usr/share/elasticsearch/bin/elasticsearch-setup-passwords interactive
Initiating the setup of passwords for reserved users elastic,apm_system,kibana,logstash_system,beats_system,remote_monitoring_user.
You will be prompted to enter passwords as the process progresses.
# Confirm using 'y' or 'N':
Please confirm that you would like to continue [y/N]: y

# The following will be prompted for each built-in user:
Enter password for [user]: [Enter a password here]
Reenter password for [user]: [Repeat the password here]

# When the shell is completed, you can exit the container:
sh-4.2# exit
```  
  
Now that everything is configured, you may restart the containers to access Kibana:
```shell
# Move to the docker-compose directory
$ cd ~/ml4imDocker/install/docker-compose
# Start the containers (You can use the -d parameter to stop logging)
$ docker-compose up
```  
