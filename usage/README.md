# Using Machine Learning For Integration Monitoring
[[_TOC_]]  
  
---
The ML4IM is setup using a docker-compose configuration. The ELK stack uses (currently) latest version of Elasticsearch, Kibana and Logstash avaiable. The Oracle XE container uses an Oracle 18c Dockerfile created using the official [Oracle docker-images documentation](https://github.com/oracle/docker-images/blob/master/OracleDatabase/SingleInstance/README.md#running-oracle-database-18c-express-edition-in-a-docker-container). When launched it will create the following containers:  
  
| Container     | Name          | Image                                                                                                                                                                                     | Port |
|---------------|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|
| Elasticsearch | elasticsearch | docker.elastic.co/elasticsearch/elasticsearch:7.7.0                                                                                                                                       | 9200 |
| Kibana        | kibana        | docker.elastic.co/kibana/kibana:7.7.0                                                                                                                                                     | 5601 |
| Logstash      | logstash      | docker.elastic.co/logstash/logstash:7.7.0                                                                                                                                                 | /    |
| Oracle XE     | oracledb      | [Custom 18XE Docker Image](https://github.com/oracle/docker-images/blob/master/OracleDatabase/SingleInstance/README.md#running-oracle-database-18c-express-edition-in-a-docker-container) | 1521 |
  
## Requirements
- Using this solution requires you to have installed both the [ML4IM Development Desktop](./../ml4im_dev_desktop/README.md) (or a similar system with the required tools) and the [ML4IM AWS Virtual Machine](./ml4im_docker/README.md).  
- Make sure the AWS Virtual Machine is running using the [EC2 Management Console](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:sort=instanceId).  
  
## Important Notes
- When restarting the AWS Virtual Machine from the EC2 Management Console the public IP adress might change. **Mind this as it may cause connection issues**.
- The first startup of Oracle XE will setup the database. This requires some time, make sure to check the logs before connecting to make sure the database is ready. **Wait until the "DATABASE IS READY TO USE!" message before attempting to use the Oracle database.**
    
## Password Configuration
> The passwords should be configured before attempting to launch the ML4IM docker-containers.  
  
The following credentials and users are setup within ML4M:  
  
| Container            | User       | Password  | Notes                            | Permissions                                                           |
|----------------------|------------|-----------|----------------------------------|-----------------------------------------------------------------------|
| Elasticsearch/Kibana | elastic    | ml4im_i8c | Built-in Superuser               | Default                                                               |
| Elasticsearch/Kibana | elastic_ml | ml4im_i8c | Superuser with ML permissions    | machine_learning_admin, machine_learning_user, superuser, kibana_user |
| Oracle XE            | sysdba     | ml4im_i8c | /                                | /                                                                     |
| Oracle XE            | demo       | ml4im_i8c | Demo user to explore/import data | DBA,IMP_FULL_DATABASE                                                 |  
  
If you wish to change the default password used in the ML4IM solution you can change them by running the `password_config.sh` script in `ml4im_docker/install/docker-compose/secrets`. This script will update all credentials required in the configuration files.  

> **Oracle requires the following requirements to be met**:
> - Should be at least 8 characters in length
> - Contain at least 1 uppercase character
> - Contain at least 1 lower case character
> - Contain at least 1 digit [0-9]
  
```shell
# Connect to the AWS Virtual Machine
$ docker-machine ssh ML4IMDocker

# Start the password_config.sh script
$ cd  ~/ml4imDocker/ml4im_docker/install/docker-compose/secrets/
$ ./password_config.sh
Enter the new password for Kibana users Elastic & Elastic_ML:
# Enter a new password
Enter the new password for Oracle users sysdba & demo:
# Enter a new password
Updating credentials..
```

## Setup   
The setup of the ML4IM has been automated to make it more user-friendly and efficient. If you wish to manually configure the ML4IM project or would like to know the inner workings, you can refer to the [Additional Information](#Project_Configuration_Explained) section.  
  
To start the docker environment, you must first open a shell on the AWS Virtual Machine:  
```shell
$ docker-machine ssh ML4IMDocker
```
  
### 1. Starting The Docker Environment  
We can start up the containers using docker-compose:  
```shell
# Move to the docker-compose directory
$ cd ~/ml4imDocker/ml4im_docker/install/docker-compose
# Start the containers (The -d parameter stops logging)
$ sudo docker-compose up
```  
> Logstash may show errors during this time, as it tries to execute SQL requests while the database is being prepared.
  
This will start up a container for Elastic Search, Kibana, Oracle XE & logstash. It will take a while for Oracle to prepare the database, wait for the "*DATABASE READY TO USE*" message in the docker-compose logs before trying to connect. The following scripts and provisions are used to prepare the ML4IM solution:  
  
<img src="./../assets/ml4im_docker_config.PNG"  width="80%">

**The elastic_setup.sh script is not executed by docker-compose and should be manually executed later.*

You may access the machine using the following ports:  
  
| Container     | Image                                                       | Port |
|---------------|-------------------------------------------------------------|------|
| Elasticsearch | docker.elastic.co/elasticsearch/elasticsearch:7.7.0         | 9200 |
| Kibana        | docker.elastic.co/kibana/kibana:7.7.0                       | 5601 |
| Logstash      | docker.elastic.co/logstash/logstash:7.7.0                   | /    |
| Oracle XE     | [kempesa/oracle_xe_18c:osels](Custom image for Oracle 18XE) | 1521 | 
  
  
### 2. Exploring Data With SQL Developer (Optional) 
> This is configured on the Development Desktop using Oracle SQL Developer. If your SQL Developer has issues starting up, make sure the right JDK version and path is defined in `/home/vagrant/.sqldeveloper/19.4.0/product.conf`. This should be `/usr/lib/jvm/java-1.8.0-openjdk-[latest version].x86_64`.  
  
> IMPORTANT: Make sure these requirements are met before executing the following step:
> - Database setup has been completed and oracle has logged the *"DATABASE READY TO USE"* message.  

First you must configure the connection to the Oracle XE Database. Click on the + symbol in the "Connections" window in SQL Developer.
You must enter the following parameters:   
- Name:  Orace XE (= Connection name, you may choose something else)
- Username: demo (= The user we created in the previous section)  
- Password:  ml4im_i8c (= This should be the same as you configured for the demo user)  
- Role: default
- Hostname: [The public IP address of the AWS Server. This can be found in the [EC2 Management Console](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:sort=instanceId)]
- Port: 1521 (= Default port)  
- SID: xe  
  
You can test the connection using the "Test" button. If the test is successful, press connect to confirm the connection.  

![SQL Configuration Example](./../assets/sqlConfig.PNG)  
  
You should now be able to open the connection and see the LOGGING_EVENTS & INTERFACE_INSTANCES table under the `Table` section.  
    
### 3. Implementing Elastic Configuration
> IMPORTANT: Make sure these requirements are met before executing the following step:
> - Database setup has been completed and oracle has logged the *"DATABASE READY TO USE"* message.
> - Logstash has transported data atleast *once* after datebase setup completion. (Log example: *"select * from DEMO.INTERFACE_INSTANCES"*). Logstash transports data every 5 minutes. 

The `setup_aws.sh` script provided at `/ml4im_docker/install/docker-compose/config/elastic` will implement all configurations required within Elasticsearch. This will automate the creating of objects, indexes, calendar management and machine learning jobs. 

```shell
# You will have to open a new terminal if you are running docker-compose (without -d) in the previous section.
# Executing the script
$ cd ~/ml4imDocker/ml4im_docker/install/docker-compose/config/elastic
$ ./elastic_setup.sh
```  
  
### 4. Explore Machine Learning Jobs
  
![](./../assets/ml4im_elastic_machinelearningJob.PNG)
  
All that is left to do is to start the datafeed for the pre-configured machine learning jobs. You can connect to Kibana on http://[AWS_IP]:5601 using the *elastic_ml* user and default password *ml4im_i8c*. Use the navaigation sidebar to open the machine learning page. Press the `Manage jobs` button on the ML overview to open the pre-configured job group. Use the option menu to the right of a machine learning job and click `Start datafeed`. After some time the processed document counter will go up. When all documents are imported you may look at the analysis using the `Anomaly Explorer`. Mind that some jobs will import less documents according to their datafeed configuration.  
  
## Additional Information  
### Elastic Machine Learning  
Elasticsearch has a wide variety of functions available to create machine learning jobs. This allows you to get more useful information out of your Elasticsearch indexes and find anomolies. The following section will give more explanation on the implementation of machine learning as ease may vary with the complexity of your data.  
  
![](./../assets/ml4im_elastic_machinelearningExampleTypes.PNG)
*The picture above shows the anomaly explorer for the machine learning job. Here we can find useful information that Elasticsearch found in our data.*  
  
**Anomaly Timeline**  
The anomaly timeline gives a visual representation on anomalies within our data. The main timeline is a combination of all types of processes and shows overall major anomalies. Below it is a breakdown of the anomaly data for each type of process in our logging data. Due to our configration of the machine learning job, anomaly data can be viewed by type, hostname or status. This gives us the oppertunity to find if certain hosts contain more anomalies than others for example.  
  
**Influencers**  
To the left of the timelines you can find the influencers of the machine learning job. Influencers were configured that we found may influence anomalies within the logging data. Elasticsearch gives us a visual representation on what influencers have the most anomalies and what you should watch out for.  
  

![](./../assets/ml4im_elastic_machinelearningExampleWarnings.PNG)
*The anomaly list used to break down time-buckets in the anomaly timeline.*   
  
**Anomaly List**    
When clicking on a time-bucket within our anomaly timeline, Elasticsearch will filter the anomaly list below to that time-bucket. This shows individual anomalies found at that time and their general information. By opening an anomaly we can find more specific information. This allows us to use Kibana to find the events in our LOGGING_EVENTS index and further research on what exactly happend and more importantly, how to prevent it from happening again.  
  
![](./../assets/ml4im_elastic_machinelearningExampleTypes2.PNG)
*A time-series with detected anomalies from the high-count machine learning job.*  
**Time Series**  
If a detector within a temporal machine learning job makes use of value datatypes, it has the feature to visualize anomalies in a time-series. This is the case for our second machine learning job as it deals with the number of incoming events within a certain time-frame. Visualization can be split into multiple series as seen above, or filtered on the given type.  
  
![](./../assets/ml4im_elastic_machinelearningDowntime.PNG)

**Dealing with downtime**  
Systems often have to deal with planned downtime on certain parts of the process. As these false negatives may influence machine learning jobs there is a function within elastic that allows us to prevent this. The calendar management systems may be used to add times of planned outages that machine learning jobs will ignore in their analysis. This makes sure that only real anomalies are detected. Within our demonstration data we have provided a pre-configured calendar that deals with planned downtime within the demonstration data.
  
---
  
#### Machine learning types  
**Temporal anomaly detection**
This is the default behaviour of machine learning within Elasticsearch. It is a comparison of the behavior of an entity with respect to itself over time. It is used to compare the **individual** behaviour of data.
  
**Specifications:**  
- Data may be split using the `by_field_name` or/and `partition_field_name`.
- Not suitable for very sparse data as it cannot create a good baseline to detect anomalies.
- Not suitable for high cardinality in data as each split in data gets its own machine learning model. This may impact performance heavily.
  
  
**Population anomaly detection**  
Population anomaly detection is only invoked if the *Population Wizard* is used when creating jobs or when using the `over_field_name` field. It is a comparison of an individual entity against a collective model of all peers as witnessed over time. It is used to compare an individual against the **collective** behaviour of data.
  
**Specifications:**  
- Data may be split using the `by_field_name` or/and `partition_field_name` to create sub-populations.
- Suitable for sparse or high cardinality data as it uses the entire collective to compare data. This will use less system resources.
  
As population anomaly detection will compare individuals to the collective data, it will detect outliers against that collective.  
  

**Example use case**    
Imagine we would like to track the number of documents downloaded from an internal document server and let’s assume that the document server contains documents that are broadly applicable to everyone in the company (a large company with 50,000+ employees). Additionally, anyone can access this document server at any time. Finally, let’s also assume that the document server’s access log tracks every time a document is accessed, as well as the user making the download.  

**Temporal Anomaly Detection**   
- Track the volume of downloads, for each user, by name, individually over time.
- If **that person** typically downloads 50 documents per day, it will be anomalous if he/she downloads documents 5000+ on a single day.
- If a new person with no history downloads 5000+ documents on a single day, it **will not** be anomalous as there is not history to base on.
  
**Population Anomaly Detection**   
- Track the volume of downloads, for all users within a certain time bucket, collectively.
- If **all users** using the system typically download 50 documents per day, it will be anomalous if he/she downloads documents 5000+ on a single day.
- If a new person with no history downloads 5000+ documents on a single day, it **will** be anomalous as it is unusual compared to the collective.
  
This section is based of an [Elastic blog post](https://www.elastic.co/blog/temporal-vs-population-analysis-in-elastic-machine-learning), it is advised you read this document as it goes more into detail.
  
#### Analysis configuration 
The Advanced machine learning tool allows users to create custom ML jobs for an Elasticsearch index. The following tabs are available to use:  
- **Job Details**: This tab is to set general information of the machine learning job. You may also set a job group here which is useful if you have are dealing with certain downtime in your data and configured a calendar for a job group.  
- **Analysis Configuration**: The most important tab, this allows you to configure the behaviour of the ML job. Here you may configure how machine learning may detect anomalies within your data.  
- **Datafeed**: This tab allows you to set the input data of the machine learning job. You can select a full index or execute a custom querry to get specific data. Make sure the timemstamp field set here is correct.  
- **Edit JSON**: This tab is used to configure the machine learning job using a JSON template. This can prove useful for exporting or importing machine learning jobs.  
- **Data Preview**: This tab previews the datafeed that will be imported into the machine learning job. This will depend on your configuration in the *Datafeed* tab.

The *Analysis Configuration* tab is used to set the behaviour of a machine learning job using functions within detectors and influencers. It also provides the functon to set a bucket span, a summary field and a categorization field.  
  
- **bucket_span**: The size of the interval that the analysis is aggregated into. Elastic will aggregate events in a time-bucket to improve performance and reduce noise. The time set may vary on how sparse or frequent events are.
- **summary_count_field_name**: This field should only be used if the index already contains a field that summarises a count of events. It cannot be used with the `metric` function.
- **categorization_field_name**: This field should only be used if you are going to create a **categorization** detector, as the field set will be categorized. The resulting categories must be used in a detector by setting `by_field_name`, `over_field_name`, or `partition_field_name` to the keyword *mlcategory*.
  
**Detectors**  
When creating machine learning jobs, the behaviour of that job is mostly based on what detectors you configure. A detector defines the fields and functions used for analyzing data and detecting anomalies. A machine learning job may have multiple detectors.  
  
![](./../assets/ml4im_elastic_machinelearningDetector.PNG)
  
- **function**: The function field sets the main functionality of the detector. This may be counting, detecting variances, categorization, etc... Depending on what you set as the `function` field, you may use or not use the other fields in a detector. Make sure to check out the documentation for a full list of all functions.  
- **field_name**: The field that is used by the provided `function`. If you need to base the `function` on the rate of events, leave this field empty. `field_name` cannot contain double quotes or backslashes.  
- **by_field_name**: The field used to soft-split the data. In particular, this property is used for analyzing the splits with respect to their own history. 
- **partition_field_name**: The field used to segment the analysis (= hard-split the data). When you use this property, you have completely independent baselines for each value of this field.
- **over_field_name**: This field should be used when using **"Population Anomaly Detecting"**. This will segment data to compare individuals to the collective population.  
- **exclude_frequent**: This field is used to exclude frequent events in the data. Entities can be considered frequent over time or frequent in a population. If you are working with both *over* and *by* fields, then you can set `exclude_frequent` to *all* for both fields, or to *by* or *over* for those specific fields.
  
You can find more information in the [documentation](https://www.elastic.co/guide/en/elasticsearch/reference/7.3/ml-job-resource.html#ml-detectorconfig).
      
**Splitting data**  
In many cases the `by_field_name`, `partition_field_name` and `over_field_name` may look very similar. There are some key differences that will change the behaviour of your machine learning job. `by_field_name` and `partition_field_name` can be used to soft-split and hard-split data. Their behaviour is different when first receiving a new type of the field specified. `over_field_name` is used to configure jobs with Population Anomaly Detection.
  
- `by_field_name` (soft-split)
  - May detect an anomaly as it has never seen this type of that field.
  - The field chosen should have < 100,000 distinct values per job, in general.
  - More appropriate for dependent attributes of an entity.
  - Scoring considers the history of other by-fields. 
  
  
- `partition_field_name` (hard-split)
  - Will see it as an expansion of the data and start a new independent analysis.
  - The field chosen should have < 10,000 distinct values per job, in general as more memory is required to partition.
  - Each instance of the field is like an independent variable
  - Scoring of anomalies for partitions is more independent.
  
> `by_field_name` and `partition_field_name` can still be used in combination to perform a double split.
(Example: count by error_code partition=host)
  
Using `over_field_name` will set the machine learning job to Population Anomaly Detection. It is used to compare individuals to the collective. Individuals are segmented using the field specified in `over_field_name`. This may still be used with `partition_field_name` to create sub-populations.  
  
Based on a [post from an Elastic Engineer](https://discuss.elastic.co/t/ml-kibana-difference-between-by-field-name-and-partition-field-name/193385/4).  
  
**Influencers**  
You may select fields that you think will influence anomalies within your index. This will change the behaviour of your machine learning jobs. Typically any `by_field_name`, `over_field_name` or `partition_field_name` fields should be set as an influencer. It is advised you set 1-3 influencers for a job.
  
---
  
### Project Configuration Explained
The guide below can be used to manually configure the ML4IM project and customize it to your own needs. It will cover every configuration that is done automatically using docker-compose and the elastic_setup script.  

#### Preparing the Database  
**Database configuration**  
Before we import the data using the Oracle Datapump, we must adjust some settings. The `oracle_01_setup.sql` is executed after the initial docker database setup and takes care of this for us.  

```sql
/* Creating the DEMO user. It receives DBA & IMPORT DATABASE PERMISSIONS. */
alter session set "_ORACLE_SCRIPT"=true;
create user demo identified by "ml4im_i8c";
grant dba to demo;
grant IMP_FULL_DATABASE to demo;
/* Creating a directory for the ML4IM_DATA.DMP to be copied to. */
create directory IMPORT_DATA as '/import_data';
/* Adjusting the temporary tablespace to prevent disk filling up. */
alter database tempfile '/opt/oracle/oradata/XE/temp01.dbf' drop including datafiles;
create temporary tablespace temp_new tempfile '/opt/oracle/oradata/XE/temp01.dbf' size 500m autoextend on next 250m maxsize 5096m;
alter database default temporary tablespace temp_new;
COMMIT;
```
  
**Importing the demonstration data**   
The ML4IM project comes with its own demo database. Available is the LOGGING_EVENTS table that is filled with anonymized logging data, aswell as the INTERFACE_INSTANCES table in which the logging data has been compressed in a single row with its last known state.  
  
**LOGGING_EVENTS**  
  
| LOGID                            | CORRELATIONID                    | TYPE          | SERVICE      | STATUS          | MESSAGE                      | CUSTOM_1     | CUSTOM_2           | CUSTOM_3            | HOSTNAME     | USERNAME     |
|----------------------------------|----------------------------------|---------------|--------------|-----------------|------------------------------|--------------|--------------------|---------------------|--------------|--------------|
| number                           | number                           | varchar2(100) | varchar(100) | varchar(20)     | varchar(500)                 | varchar(100) | varchar(100)       | varchar(100)        | varchar(100) | varchar(100) |
| 20200317054642418152162733352503 | 20200317054642145434195210797011 | Type 1        | Service 1    | interface_start | Hidden details for service 1 | System 1     | Hidden Document ID | Hidden Interface ID | Host 1       | User 1       |  
  
**INTERFACE_INSTANCES**  
  
| CORRELATIONID                    | TYPE          | START_TIMESTAMP | STOP_TIMESTAMP | SERVICE      | STATUS          | MESSAGE                      | CUSTOM_1     | CUSTOM_2           | HOSTNAME     | USERNAME     |
|----------------------------------|---------------|-----------------|----------------|--------------|-----------------|------------------------------|--------------|--------------------|--------------|--------------|
| number                           | varchar2(100) | TIMESTAMP(9)    | TIMESTAMP(9)   | varchar(100) | varchar(20)     | varchar(500)                 | varchar(100) | varchar(100)       | varchar(100) | varchar(100) |
| 20200317054642145434195210797011 | Type 1        | 17-MAR-20       | 17-MAR-20      | Service 1    | interface_ready | Hidden details for service 1 | System 1     | Hidden Document ID | Host 1       | User 1       |  
  
Both tables are imported by running the `oracle_02_import.sql` script inside the docker-container. This script is run after initial docker database setup. It makes use of the Oracle Datapump to extract the tables from the `ML4IM_DATA.DMP` file.

```shell
# Setting permissions on the ORACLE binaries.
cd $ORACLE_HOME/bin
chmod 6751 oracle
# Importing the tables from the datadump.
impdp demo/ml4im_i8c TABLES=demo.interface_instances,demo.logging_events DUMPFILE=import_data:ML4IM_DATA.DMP NOLOGFILE=YES
```  
    
#### Preparing Elasticsearch
Elasticsearch configuration is implemented by using the `elastic_setup.sh` shell script. It automates the creating of indexes, users and machine learning methods.  
  
```shell
#!/bin/bash
printf "### Starting setup ###\n"
# Load configured password
elastic_pwd=ml4im_i8c

# Creating the Demo user
printf "### Creating elastic_ml user.. ###\n"
curl -u elastic:$elastic_pwd -X POST "localhost:9200/_security/user/elastic_ml?pretty" -H 'Content-Type: application/json' -d'{"password" : "$elastic_pwd","roles" : [ "machine_learning_admin", "machine_learning_user", "superuser", "kibana_user" ],"full_name" : "ML4IM Demo User"}'

# Starting a 30-day trail

printf "### Starting trail license.. ###\n"
curl -u elastic_ml:$elastic_pwd -X POST "localhost:9200/_license/start_trial?acknowledge=true&pretty"

# Creating the index patterns

printf "### Creating index patterns.. ###\n"
curl -u elastic_ml:$elastic_pwd -X POST "localhost:5601/api/saved_objects/_import" -H "kbn-xsrf: true" --form file=@indexPatterns.ndjson

# Creating the machine learning - calendar

printf "### Creating ML calendar & implementing downtime data.. ###"
curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/calendars/planned-downtime?pretty" -H 'Content-Type: application/json' -d '{"calendar_id": "planned-downtime","job_ids": ["planned-downtime"]}'

curl -u elastic_ml:$elastic_pwd -X POST "localhost:9200/_ml/calendars/planned-downtime/events?pretty" -H 'Content-Type: application/json' -d '{"events" : [{"description" : "downtime_01","start_time" : 1583168400000,"end_time" : 1583820000000},{"description" : "downtime_02","start_time" : 1584458490000,"end_time" : 1584684000000},{"description" : "downtime_03","start_time" : 1585325700000,"end_time" : 1585551600000}]}'

# Creating the machine learning - jobs

printf "### Creating machine learning jobs.. ###\n"
curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/anomaly_detectors/ml_time_variance_by_type?pretty" -H 'Content-Type: application/json' -d '{ "job_id": "ml_time_variance_by_type", "job_type": "anomaly_detector", "job_version": "7.3.0", "groups": [ "planned-downtime" ], "description": "[DEMO] Advanced - Checks high variance in time_passed by type", "analysis_config": { "bucket_span": "15m", "detectors": [ { "detector_description": "time_variance_by_type", "function": "high_varp", "field_name": "time_passed", "by_field_name": "type", "detector_index": 0 } ], "influencers": [ "hostname", "status", "type" ] }, "analysis_limits": { "model_memory_limit": "1024mb", "categorization_examples_limit": 4 }, "data_description": { "time_field": "start_timestamp", "time_format": "epoch_ms" }, "model_snapshot_retention_days": 1, "results_index_name": "shared" }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/anomaly_detectors/ml_completed_time_variance_by_type?pretty" -H 'Content-Type: application/json' -d '{ "job_id": "ml_completed_time_variance_by_type", "job_type": "anomaly_detector", "job_version": "7.3.0", "groups": [ "planned-downtime" ], "description": "[DEMO] Advanced - Checks high variance in time_passed by type, only analyzing interfaces that have been succesful & completed.", "analysis_config": { "bucket_span": "15m", "detectors": [ { "detector_description": "time_variance_by_type", "function": "high_varp", "field_name": "time_passed", "by_field_name": "type", "detector_index": 0 } ], "influencers": [ "hostname", "status", "type" ] }, "analysis_limits": { "model_memory_limit": "1024mb", "categorization_examples_limit": 4 }, "data_description": { "time_field": "start_timestamp", "time_format": "epoch_ms" }, "model_snapshot_retention_days": 1, "custom_settings": { "custom_urls": [] }, "results_index_name": "shared" } }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/anomaly_detectors/ml_count_events_by_type?pretty" -H 'Content-Type: application/json' -d '{ "job_id": "ml_count_events_by_type", "job_type": "anomaly_detector", "job_version": "7.3.0", "groups": [ "planned-downtime" ], "description": "[DEMO] Advanced - Checks high count of events by type", "analysis_config": { "bucket_span": "15m", "detectors": [ { "detector_description": "high_count by type", "function": "high_count", "by_field_name": "type", "detector_index": 0 } ], "influencers": [ "type", "status", "hostname" ] }, "analysis_limits": { "model_memory_limit": "1024mb", "categorization_examples_limit": 4 }, "data_description": { "time_field": "start_timestamp", "time_format": "epoch_ms" }, "model_snapshot_retention_days": 1, "results_index_name": "shared" }'

# Creating the machine learning - datafeeds

printf "### Implementing datafeeds for machine learning jobs.. ###\n"
curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/datafeeds/datafeed_ml_time_variance_by_type" -H 'Content-Type: application/json' -d '{ "datafeed_id": "datafeed_ml_time_variance_by_type", "job_id": "ml_time_variance_by_type", "indices": [ "interface_instances" ], "query": { "match_all": {} }, "chunking_config": { "mode": "auto" }, "delayed_data_check_config": { "enabled": true } }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/datafeeds/datafeed-ml_completed_time_variance_by_type" -H 'Content-Type: application/json' -d '{ "datafeed_id": "datafeed-ml_completed_time_variance_by_type", "job_id": "ml_completed_time_variance_by_type", "query_delay": "117666ms", "indices": [ "interface_instances" ], "query": { "term": { "status": "interface_ready" } }, "scroll_size": 1000, "chunking_config": { "mode": "auto" }, "delayed_data_check_config": { "enabled": true } }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/datafeeds/datafeed_ml_count_events_by_type" -H 'Content-Type: application/json' -d '{ "datafeed_id": "datafeed_ml_count_events_by_type", "job_id": "ml_count_events_by_type", "indices": [ "interface_instances" ], "query": { "match_all": {} }, "chunking_config": { "mode": "auto" }, "delayed_data_check_config": { "enabled": true } }'
printf "### Elastic Configuration Completed ###\n"
```
  
All these commands are executed using CURL. These can however also be executed within Kibana using the `Dev Tools`. You can do this by going to ([Your AWS VM IP Address]:5601), logging in with your credentials (default: elastic - ml4im_i8c) and going to the `Dev Tools` section (The wrench icon on the navigation bar).  
  
**The following section will explain each step within the** `elastic_setup.sh` **script:**  
  
- The `elastic_setup.sh` will create a user with the following properties:  
![](./../assets/elasticUser.PNG)  

You may do this manually by logging into kibana at http://[AWS_VM_IP_ADDRESS]:5601 using the built-in superuser *elastic*. In the navigation bar use the gear icon to go to the *Management* page. Afterwards go to the *Users* section under *Security*.  
  
Here you can manage the users and permissions within Kibana. Press the `Create User` button and create the user using the settings shown. Make sure the permissions are correct. This user can be used in the future to create machine learning jobs.  
  
- The `elastic_setup.sh` will create 2 index objects using the following template:  
  
*Elasticsearch - Log Events*   
It contains the log events, transformed for ease of use.  

| logid                            | correlationid                    | message                      | timestamp                   | status          | service   | type    | custom1  | custom2            | custom3             | hostname | username |
|----------------------------------|----------------------------------|------------------------------|-----------------------------|-----------------|-----------|---------|----------|--------------------|---------------------|----------|----------|
| keyword                          | keyword                          | text                         | date                        | keyword         | keyword   | keyword | text     | text               | text                | keyword  | keyword  |
| 20200317054642418152162733352503 | 20200317054642145434195210797011 | Hidden details for service 1 | Feb 26, 2020 @ 11:06:51.330 | interface_start | Service 1 | Type 1  | System 1 | Hidden Document ID | Hidden Interface ID | Host 1   | User 1   |  
  
*Elasticsearch - Interface Instances*    
It contains a document for each interface_instance_id. This document is the latest status of the interface, the latest log. The field time_passed also reflects how long an interface has been active (ms). 
 
| correlationid                    | message                      | start_timestamp             | end_timestamp               | time_passed | status          | type    | custom1  | custom2            | hostname | username |
|----------------------------------|------------------------------|-----------------------------|-----------------------------|-------------|-----------------|---------|----------|--------------------|----------|----------|
| keyword                          | text                         | date                        | date                        | integer     | keyword         | keyword | text     | text               | keyword  | keyword  |
| 20200317054642418152162733352503 | Hidden details for service 1 | Feb 26, 2020 @ 11:06:51.330 | Feb 26, 2020 @ 11:06:51.881 | 551         | interface_ready | Type 1  | System 1 | Hidden Document ID | Host 1   | User 1   |
  
You can find more information on creating your own index scripts in the [Index API documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html).
If you find yourself in a sitation where you want to delete all documents inside a given index without deleting the index pattern, you can use the following script within the Kibana Dev Tools:
  
```
# Empty a given index
POST interface_instances/_delete_by_query
{
  "query": {
    "match_all": {}
  }
}
```
  
- The `elastic_setup.sh` script will start a 30-day trail license so that machine learning functions may be used. 
  
#### Importing Machine Learning Jobs
The ML4IM project uses *Advanced Machine Learning* jobs within Elasticsearch to get useful results and will go into further detail on how to implement these. We can create these by going to the Machine Learning tab, clicking the `Create new job` button, selecting an index and selecting the *Advanced* option. The ML4IM project provides the following pre-configured machine learning jobs:  
  
- **ml_time_variance_by_type**: The goal is to find anomalies in the time passed between the start and end of a interface instance. A function is executed against the index that checks the variances in time_passed, comparing it to the temporal history of previous instances with the given type (Updates, Information gathering, etc..).  
  
- **ml_time_variance_completed_by_type**: The goal is to find anomalies in the time passed between the start and end of a completed & succesful interface instance. A function is executed against the index that checks the variances in time_passed, comparing it to the temporal history of previous instances with the given type (Updates, Information gathering, etc..). Only interfaces with status *interface_ready* are analyzed.

- **ml_count_events_by_type**: The goal is to find anomalies in the high count of starting interfaces in static time-frames. A function is executed against the index that checks the amount of events within a certain time-frame, comparing it to the temporal history of previous time-frames with the given type (Updates, Information gathering, etc..).  

  
The following jobs can be imported using the `Dev Tools`. Execute the following code to configure the necessary job group, datafeed and job configuration. The ML4IM project will import these jobs using the `elastic_setup.sh` script instead.
  
**Job group & downtime calendar**    
This code will configure the planned downtime within our data. It is important that you add planned outages to a calendar within Elasticsearch, otherwise this may influence anomaly results. More information can be found in the [Calendar API](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/ml-post-calendar-event.html) section in the documentation.  

```json
PUT _ml/calendars/planned-downtime
{
  "calendar_id": "planned-downtime",
  "job_ids": ["planned-downtime"]
}
``` 
  
```json
POST _ml/calendars/planned-downtime/events
{
  "events" : [
    {
      "description" : "downtime_01",
      "start_time" : 1583168400000,
      "end_time" : 1583820000000
    },
    {
      "description" : "downtime_02",
      "start_time" : 1584458490000,
      "end_time" : 1584684000000
    },
    {
      "description" : "downtime_03",
      "start_time" : 1585325700000,
      "end_time" : 1585551600000
    }
  ]
}

```  
     
**Job configuration**    
This code will configure the machine learning job within Elasticsearch. More information can be found in the [Create anomaly jobs](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/ml-put-job.html) section in the documentation.  
  
```json
PUT _ml/anomaly_detectors/ml_time_variance_by_type
{
  "job_id": "ml_time_variance_by_type",
  "job_type": "anomaly_detector",
  "job_version": "7.3.0",
  "groups": [
    "planned-downtime"
  ],
  "description": "[DEMO] Advanced - Checks high variance in time_passed by type",
  "analysis_config": {
    "bucket_span": "15m",
    "detectors": [
      {
        "detector_description": "time_variance_by_type",
        "function": "high_varp",
        "field_name": "time_passed",
        "by_field_name": "type",
        "detector_index": 0
      }
    ],
    "influencers": [
      "hostname",
      "status",
      "type"
    ]
  },
  "analysis_limits": {
    "model_memory_limit": "1024mb",
    "categorization_examples_limit": 4
  },
  "data_description": {
    "time_field": "start_timestamp",
    "time_format": "epoch_ms"
  },
  "model_snapshot_retention_days": 1,
  "results_index_name": "shared"
}

```  
  
```json
PUT _ml/anomaly_detectors/ml_completed_time_variance_by_type
{
  "job_id": "ml_completed_time_variance_by_type",
  "job_type": "anomaly_detector",
  "job_version": "7.3.0",
  "groups": [
    "planned-downtime"
  ],
  "description": "[DEMO] Advanced - Checks high variance in time_passed by type, only analyzing interfaces that have been succesful & completed.",
  "analysis_config": {
    "bucket_span": "15m",
    "detectors": [
      {
        "detector_description": "time_variance_by_type",
        "function": "high_varp",
        "field_name": "time_passed",
        "by_field_name": "type",
        "detector_index": 0
      }
    ],
    "influencers": [
      "hostname",
      "status",
      "type"
    ]
  },
  "analysis_limits": {
    "model_memory_limit": "1024mb",
    "categorization_examples_limit": 4
  },
  "data_description": {
    "time_field": "start_timestamp",
    "time_format": "epoch_ms"
  },
  "model_snapshot_retention_days": 1,
  "custom_settings": {
    "custom_urls": []
  },
  "results_index_name": "shared"
}
}

``` 
  
```json
PUT _ml/anomaly_detectors/ml_count_events_by_type
{
  "job_id": "ml_count_events_by_type",
  "job_type": "anomaly_detector",
  "job_version": "7.3.0",
  "groups": [
    "planned-downtime"
  ],
  "description": "[DEMO] Advanced - Checks high count of events by type",
  "analysis_config": {
    "bucket_span": "15m",
    "detectors": [
      {
        "detector_description": "high_count by type",
        "function": "high_count",
        "by_field_name": "type",
        "detector_index": 0
      }
    ],
    "influencers": [
      "type",
      "status",
      "hostname"
    ]
  },
  "analysis_limits": {
    "model_memory_limit": "1024mb",
    "categorization_examples_limit": 4
  },
  "data_description": {
    "time_field": "start_timestamp",
    "time_format": "epoch_ms"
  },
  "model_snapshot_retention_days": 1,
  "results_index_name": "shared"
}

``` 
  
**Datafeed**    
This code will configure the datafeeds for our machine learning jobs within Elasticsearch. More information can be found in the [Create datafeed API](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/ml-put-datafeed.html) section in the documentation.  
  
```json
PUT _ml/datafeeds/datafeed_ml_time_variance_by_type
{
    "datafeed_id": "datafeed_ml_time_variance_by_type",
    "job_id": "ml_time_variance_by_type",
    "indices": [
      "interface_instances"
    ],
    "query": {
      "match_all": {}
    },
    "chunking_config": {
      "mode": "auto"
    },
    "delayed_data_check_config": {
      "enabled": true
    }
}

```  
  
```json
PUT _ml/datafeeds/datafeed-ml_completed_time_variance_by_type
{
    "datafeed_id": "datafeed-ml_completed_time_variance_by_type",
    "job_id": "ml_completed_time_variance_by_type",
    "query_delay": "117666ms",
    "indices": [
      "interface_instances"
    ],
    "query": {
      "term": {
        "status": "interface_ready"
      }
    },
    "scroll_size": 1000,
    "chunking_config": {
      "mode": "auto"
    },
    "delayed_data_check_config": {
      "enabled": true
    }
}

```

```json
PUT _ml/datafeeds/datafeed_ml_count_events_by_type
{
    "datafeed_id": "datafeed_ml_count_events_by_type",
    "job_id": "ml_count_events_by_type",
    "indices": [
      "interface_instances"
    ],
    "query": {
      "match_all": {}
    },
    "chunking_config": {
      "mode": "auto"
    },
    "delayed_data_check_config": {
      "enabled": true
    }
}

```
  
After executing all sections of code above, the machine learning jobs should be fully configured. You can check this be looking at the `Job Management` section on the *Machine Learning* page.  

#### Configuring Logstash
Logstash transports data from our Oracle Database to Elasticsearch. The ML4IM project provides a sample configuration file for logstash so that you may customize it to your needs. The same configuration file can be found at `/ml4im_docker/install/docker-compose/config/logstash`.

**Configuration file structure**   
**Input**    
The first section allows you to input data into Logstash. Logstash can extract data from many sources, but we will be using the Oracle Database. The pre-installed JDBC plugin allows us to do this easily. You may put multiple input sources in the input section.  
  
- **tags**: This custom paramater is only required if you will be using multiple inputs, filters or outputs. You may use this to name your input.
- **jdbc_connection_string**: This connection string will be used to connect to the local Oracle Database. You must provide in the public IP address of the virtual machine.  
- **jdbc_user & jdbc_password**: These are the credentials used to log into the Oracle Database. In the case of this tutorial you must use *sys as sysdba* as the user and the password you have set previously.
- **jdbc_driver_library**: The location of the JDBC driver inside the docker container. If you have downloaded this file as requested in the first tutorial then docker has copied this correctly to the `/ojdbc8/` directory.  
- **statement**: This is the SQL statement that will be run against the Oracle Database. In this case you will simply extract all rows of the table given using a select statement. Make sure to change the *myTable* to the table name.  
- **schedule**: This syntax will configure when Logstash should repeat the SQL statement. In the example shown below the logstash configuration will be repeated every 5 minutes.  

```ruby
 jdbc {
       tags => ["log"]
       jdbc_validate_connection => true
       jdbc_connection_string => "jdbc:oracle:thin:@oracledb:1521/xe"
       jdbc_user => "demo"
       jdbc_password => "ml4im_i8c"
       jdbc_driver_library => "/usr/share/logstash/logstash-core/lib/jars/ojdbc8.jar"
       jdbc_driver_class => "Java::oracle.jdbc.driver.OracleDriver"
       statement => "select logid, to_char(correlationid) as correlationid, message, type, service, status, custom_1, custom_2,
       custom_3, hostname, username from DEMO.LOGGING_EVENTS"
       schedule => "*/5 * * * *"
  }

 jdbc {
       tags  => ["interface"]
       jdbc_validate_connection => true
       jdbc_connection_string => "jdbc:oracle:thin:@oracledb:1521/xe"
       jdbc_user => "demo"
       jdbc_password => "ml4im_i8c"
       jdbc_driver_library => "/usr/share/logstash/logstash-core/lib/jars/ojdbc8.jar"
       jdbc_driver_class => "Java::oracle.jdbc.driver.OracleDriver"
       statement => "select * from DEMO.INTERFACE_INSTANCES"
       schedule => "*/5 * * * *"
  }
  
}
````
  
More information on the JDBC input plugin can be found [here](https://www.elastic.co/guide/en/logstash/current/plugins-inputs-jdbc.html).
  
**Filter**    
The filter section allows you to transform the incoming data, create new fields or mutate existing ones. 
By default a ruby filter is applied to the incoming data. This will use the demo data to create a new field that contains the difference between start- & end-timestamps in milliseconds for the interface data, as also overwriting the timestamp value for log events. If you do not use the demo-data, you must disable or remove this filter.
  
- **The logging data**: The exisiting ID contains a numeric code that translates to a timestamp. This is converted to an integer using the *mutate* filter. A temporary field *timestamp* is created that holds this value using a ruby filter. The last filter *date* converts the numeric value to a timestamp and sets the default *@timestamp* field in elastic. The temporary field is removed afterwards.
  
- **The interface data**: The configation below calculates a *time_passed* field that contains the difference between the start- and end-timestamp. It is calculated and set using a *ruby* filter.
  
```ruby
filter {

  if "log" in [tags] {
    mutate {
		convert => { "logid" => "integer" }
    }

    ruby {
		code => "event.set('timestamp', event.get('logid').to_s[0..16])"
    }

    date {
		match => ["timestamp", "yyyyMMddHHmmssSSS"]
		remove_field => "timestamp"
    }

  }

  if "interface" in [tags] {
	mutate {
		convert => {"correlationid" => "integer"}
	}

    ruby {
      init => "require 'time'"
      code => "event.set('time_passed', (event.get('end_timestamp') - event.get('start_timestamp')).to_f * 1000)"
    }
  }
}
```
  
More information on the filter section and transforming data can be found [here](https://www.elastic.co/guide/en/logstash/current/transformation.html). 
      
**Output**    
The last section in the configuration file is the ouput. This section defines where data should end up going. You may use multiple outputs inside this section.
  
- **if "interface" in [tags]**: This if statement is only required if you are using multiple inputs and you must assign inputs to outputs. Make sure the input *tag* is the same as defined in this statement to assign it.
- **hosts**: The Elasticsearch URL. Make sure to provide the public IP address of the virtual machine.
- **index**: The index in which the data should be inserted. If the index does not exist, a new index will be created.
- **user & password**: The credentials used to log into kibana. You should use the built-in user *elastic* and the password you set in the previous section.
- **document_id**:  The unique id for each document within the given index. You should use the primary key in the table you are going to insert. This will prevent duplicates inside the index when logstash is transporting data.  
- **action**: This defines which action should be taken when sending data to the output. When this parameter is not present, logstash will only create new documents for each document_id. When set to *update*, it will update existing documents. You can use this in combination with *doc_as_upsert*.  
- **doc_as_upsert**: When set to true, logstash will both insert and update (= upsert) documents in Elasticsearch. This will impact performance.  
- **stdout{}**: This should only be used when debugging logstash. It will export all rows to the logging terminal for inspection. 
- **template & template_name**: See section below.   
```ruby
output {

if "log" in [tags] {
  elasticsearch {
    hosts => ["http://elasticsearch:9200"]
    index => "log_events"
    user => "elastic"
    password => "ml4im_i8c"
    document_id => "%{[logid]}"
    action => "update"
    doc_as_upsert => true
    template => "/config-dir/log_index.json"
	  template_name => "log_events"
  }
}

if "interface" in [tags] {
  elasticsearch {
    hosts => ["http://elasticsearch:9200"]
    index => "interface_instances"
    user => "elastic"
    password => "ml4im_i8c"
    document_id => "%{[correlationid]}"
    action => "update"
    doc_as_upsert => true
    template => "/config-dir/interface_index.json"
	  template_name => "interface_instances"
  }
}

## Outputs data in docker logs.
# stdout {}

}
```  
  
**Using a template to create an Index**  
Documents are kept inside an index in Elasticsearch. When Logstash sends data to Elasticsearch it searches for the index given inside the configuration file. If the given index does not exist, it will attempt to create the index itself. While this may work, it oftens creates errors due to values not fitting set datatypes.
    
The Logstash configuration uses index templates to make sure the correct data types are used within Elasticsearch. `log_index.json` & `interface_index.json` are used by the Logstash configuration to create the indexes. It should be noted that Logstash will only use the template if the incoming data matches the field names and data types, it will otherwise try and use the default template.  
    
