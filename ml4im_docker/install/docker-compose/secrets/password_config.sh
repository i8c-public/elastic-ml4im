#!/bin/bash
# Set passwords
echo "Enter the new password for Kibana users Elastic & Elastic_ML:"
read elastic_pwd

echo "Enter the new password for Oracle users sysdba & demo:"
read oracle_pwd

echo "Updating credentials.."
# docker-compose.yml
sudo sed -i "s/'ELASTIC_PASSWORD=.*'/'ELASTIC_PASSWORD=${elastic_pwd}'/g" ../docker-compose.yml
sudo sed -i "s/ORACLE_PWD=.*/ORACLE_PWD=${oracle_pwd}/g" ../docker-compose.yml

# elastic_setup.sh
sudo sed -i "s/elastic_pwd=.*/elastic_pwd=${elastic_pwd}/g" ../config/elastic/elastic_setup.sh

# logstash.conf
sudo sed -i 's/jdbc_password => ".*"/jdbc_password => "'${oracle_pwd}'"/g' ../config/logstash/logstash.conf
sudo sed -i 's/password => ".*"/password => "'${elastic_pwd}'"/g' ../config/logstash/logstash.conf

# oracle_01_setup.sql
sudo sed -i 's/identified by ".*";/identified by "'${oracle_pwd}'";/g' ../config/oracle/setup/oracle_01_setup.sql

# oracle_02_import.sh
sudo sed -i "s| demo/.* TABLES| demo/${oracle_pwd} TABLES|g" ../config/oracle/setup/oracle_02_import.sh

# secrets
sudo sed -i 's|elasticsearch.password: ".*"|elasticsearch.password: "'${elastic_pwd}'"|g' ./kibana.yml
sudo sed -i 's|xpack.management.elasticsearch.password: ".*"|xpack.management.elasticsearch.password: "'${elastic_pwd}'"|g' ./logstash.yml
