#!/bin/bash
printf "### Starting setup ###\n"
# Load configured password
elastic_pwd=ml4im_i8c

# Creating the Demo user
printf "### Creating elastic_ml user.. ###\n"
curl -u elastic:$elastic_pwd -X POST "localhost:9200/_security/user/elastic_ml?pretty" -H 'Content-Type: application/json' -d'{"password" : "'$elastic_pwd'","roles" : [ "machine_learning_admin", "machine_learning_user", "superuser", "kibana_user" ],"full_name" : "ML4IM Demo User"}'

# Starting a 30-day trail

printf "### Starting trail license.. ###\n"
curl -u elastic_ml:$elastic_pwd -X POST "localhost:9200/_license/start_trial?acknowledge=true&pretty"

# Creating the index patterns
sudo chmod 777 indexPatterns.ndjson

printf "### Creating index patterns.. ###\n"
curl -u elastic_ml:$elastic_pwd -X POST "localhost:5601/api/saved_objects/_import" -H "kbn-xsrf: true" --form file=@indexPatterns.ndjson

# Creating the machine learning - calendar

printf "### Creating ML calendar & implementing downtime data.. ###"
curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/calendars/planned-downtime?pretty" -H 'Content-Type: application/json' -d '{"calendar_id": "planned-downtime","job_ids": ["planned-downtime"]}'

curl -u elastic_ml:$elastic_pwd -X POST "localhost:9200/_ml/calendars/planned-downtime/events?pretty" -H 'Content-Type: application/json' -d '{"events" : [{"description" : "downtime_01","start_time" : 1583168400000,"end_time" : 1583820000000},{"description" : "downtime_02","start_time" : 1584458490000,"end_time" : 1584684000000},{"description" : "downtime_03","start_time" : 1585325700000,"end_time" : 1585551600000}]}'

# Creating the machine learning - jobs

printf "### Creating machine learning jobs.. ###\n"
curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/anomaly_detectors/ml_time_variance_by_type?pretty" -H 'Content-Type: application/json' -d '{ "job_id": "ml_time_variance_by_type", "job_type": "anomaly_detector", "job_version": "7.3.0", "groups": [ "planned-downtime" ], "description": "[DEMO] Advanced - Checks high variance in time_passed by type", "analysis_config": { "bucket_span": "15m", "detectors": [ { "detector_description": "time_variance_by_type", "function": "high_varp", "field_name": "time_passed", "by_field_name": "type", "detector_index": 0 } ], "influencers": [ "hostname", "status", "type" ] }, "analysis_limits": { "model_memory_limit": "1024mb", "categorization_examples_limit": 4 }, "data_description": { "time_field": "start_timestamp", "time_format": "epoch_ms" }, "model_snapshot_retention_days": 1, "results_index_name": "shared" }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/anomaly_detectors/ml_completed_time_variance_by_type?pretty" -H 'Content-Type: application/json' -d '{ "job_id": "ml_completed_time_variance_by_type", "job_type": "anomaly_detector", "job_version": "7.3.0", "groups": [ "planned-downtime" ], "description": "[DEMO] Advanced - Checks high variance in time_passed by type, only analyzing interfaces that have been succesful & completed.", "analysis_config": { "bucket_span": "15m", "detectors": [ { "detector_description": "time_variance_by_type", "function": "high_varp", "field_name": "time_passed", "by_field_name": "type", "detector_index": 0 } ], "influencers": [ "hostname", "status", "type" ] }, "analysis_limits": { "model_memory_limit": "1024mb", "categorization_examples_limit": 4 }, "data_description": { "time_field": "start_timestamp", "time_format": "epoch_ms" }, "model_snapshot_retention_days": 1, "custom_settings": { "custom_urls": [] }, "results_index_name": "shared" } }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/anomaly_detectors/ml_count_events_by_type?pretty" -H 'Content-Type: application/json' -d '{ "job_id": "ml_count_events_by_type", "job_type": "anomaly_detector", "job_version": "7.3.0", "groups": [ "planned-downtime" ], "description": "[DEMO] Advanced - Checks high count of events by type", "analysis_config": { "bucket_span": "15m", "detectors": [ { "detector_description": "high_count by type", "function": "high_count", "by_field_name": "type", "detector_index": 0 } ], "influencers": [ "type", "status", "hostname" ] }, "analysis_limits": { "model_memory_limit": "1024mb", "categorization_examples_limit": 4 }, "data_description": { "time_field": "start_timestamp", "time_format": "epoch_ms" }, "model_snapshot_retention_days": 1, "results_index_name": "shared" }'

# Creating the machine learning - datafeeds

printf "### Implementing datafeeds for machine learning jobs.. ###\n"
curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/datafeeds/datafeed_ml_time_variance_by_type" -H 'Content-Type: application/json' -d '{ "datafeed_id": "datafeed_ml_time_variance_by_type", "job_id": "ml_time_variance_by_type", "indices": [ "interface_instances" ], "query": { "match_all": {} }, "chunking_config": { "mode": "auto" }, "delayed_data_check_config": { "enabled": true } }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/datafeeds/datafeed-ml_completed_time_variance_by_type" -H 'Content-Type: application/json' -d '{ "datafeed_id": "datafeed-ml_completed_time_variance_by_type", "job_id": "ml_completed_time_variance_by_type", "query_delay": "117666ms", "indices": [ "interface_instances" ], "query": { "term": { "status": "interface_ready" } }, "scroll_size": 1000, "chunking_config": { "mode": "auto" }, "delayed_data_check_config": { "enabled": true } }'

curl -u elastic_ml:$elastic_pwd -X PUT "localhost:9200/_ml/datafeeds/datafeed_ml_count_events_by_type" -H 'Content-Type: application/json' -d '{ "datafeed_id": "datafeed_ml_count_events_by_type", "job_id": "ml_count_events_by_type", "indices": [ "interface_instances" ], "query": { "match_all": {} }, "chunking_config": { "mode": "auto" }, "delayed_data_check_config": { "enabled": true } }'
printf "### Elastic Configuration Completed ###\n"