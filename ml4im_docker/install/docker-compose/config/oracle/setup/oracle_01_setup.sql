alter session set "_ORACLE_SCRIPT"=true;
create user demo identified by "ml4im_i8c";
grant dba to demo;
grant IMP_FULL_DATABASE to demo;
create directory IMPORT_DATA as '/import_data';
alter database tempfile '/opt/oracle/oradata/XE/temp01.dbf' drop including datafiles;
create temporary tablespace temp_new tempfile '/opt/oracle/oradata/XE/temp01.dbf' size 500m autoextend on next 250m maxsize 5096m;
alter database default temporary tablespace temp_new;
COMMIT;