create or replace NONEDITIONABLE PROCEDURE logging_events_export AS

    i              NUMBER;
    temp_custom_1  VARCHAR(100);
    temp_custom_2  VARCHAR(100);
    temp_custom_3  VARCHAR(100);
BEGIN
    dbms_output.enable();
    dbms_output.put_line('Starting..');
       
    --- ANONYMIZE LOOP
    --- TYPE
    i := 0;
    FOR log IN (
        SELECT DISTINCT
            ( type )
        FROM
            logging_events
    ) LOOP
        i := i + 1;
        UPDATE logging_events
        SET
            type = concat('Type ', to_char(i))
        WHERE
            type = log.type;

    END LOOP;

    dbms_output.put_line('Type column anonymized');
    
    --- SERVICE & MESSAGE
    i := 0;
    FOR log IN (
        SELECT DISTINCT
            ( service )
        FROM
            logging_events
    ) LOOP
        i := i + 1;
        UPDATE logging_events
        SET
            service = concat('Service ', to_char(i)),
            message = concat('Anonymised details from service ', to_char(i))
        WHERE
            service = log.service;

    END LOOP;

    dbms_output.put_line('Service & message columns anonymized');
    
    --- USER
    i := 0;
    FOR log IN (
        SELECT DISTINCT
            ( username )
        FROM
            logging_events
    ) LOOP
        i := i + 1;
        UPDATE logging_events
        SET
            username = concat('User ', to_char(i))
        WHERE
            username = log.username;

    END LOOP;

    dbms_output.put_line('Username column anonymized');
    
    --- HOSTNAME
    i := 0;
    FOR log IN (
        SELECT DISTINCT
            ( hostname )
        FROM
            logging_events
    ) LOOP
        i := i + 1;
        UPDATE logging_events
        SET
            hostname = concat('Host ', to_char(i))
        WHERE
            hostname = log.hostname;

    END LOOP;

    dbms_output.put_line('Hostname column anonymized');
        
    --- CUSTOM1
    i := 0;
    
        UPDATE logging_events
        SET
            custom_1 = 'Hidden'
        WHERE
            custom_1 IS NOT NULL AND custom_1 NOT LIKE 'try:%' AND custom_1 NOT LIKE 'Instance =%' AND custom_1 NOT LIKE '%System%';
            
    FOR log IN (
        SELECT DISTINCT
            ( custom_1 )
        FROM
            logging_events
        WHERE
            custom_1 IS NOT NULL AND custom_1 LIKE 'try:%' OR custom_1 LIKE 'Instance =%' OR custom_1 LIKE '%System%'
    ) LOOP
        BEGIN
            CASE
                WHEN log.custom_1 LIKE 'try:%' THEN
                    temp_custom_1 := log.custom_1;
                WHEN log.custom_1 LIKE 'Instance =%' THEN
                    temp_custom_1 := log.custom_1;
                WHEN log.custom_1 LIKE '%System%' THEN
                    i := i + 1;
                    temp_custom_1 := concat('System ', to_char(i));
                ELSE
                    temp_custom_1 := 'Hidden';
            END CASE;
        END;

        UPDATE logging_events
        SET
            custom_1 = temp_custom_1
        WHERE
            custom_1 = log.custom_1;

    END LOOP;

    dbms_output.put_line('Custom_1 column anonymized');
        
    --- CUSTOM2
    UPDATE logging_events
    SET
        custom_2 = 'Hidden document ID'
    WHERE
        custom_2 LIKE '%Doc%';

    UPDATE logging_events
    SET
        custom_2 = 'Hidden'
    WHERE
        custom_2 NOT LIKE 'Hidden document ID'
        AND custom_2 IS NOT NULL;

    dbms_output.put_line('Custom_2 column anonymized');
    
    --- CUSTOM3
    UPDATE logging_events
    SET
        custom_3 = 'Hidden interface ID'
    WHERE
        custom_3 LIKE '%ID%';

    UPDATE logging_events
    SET
        custom_3 = 'Hidden'
    WHERE
        custom_3 NOT LIKE 'Hidden interface ID'
        AND custom_3 IS NOT NULL;

    dbms_output.put_line('Custom_3 column anonymized');
    dbms_output.put_line('Anonymization completed. Commiting changes..');
    COMMIT;
    dbms_output.put_line('Changes commited');
END logging_events_export;