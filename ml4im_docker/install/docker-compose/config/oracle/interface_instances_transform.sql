--- REMOVE ALL ROWS FROM INTERFACE INSTANCES
drop table sys.interface_instances;

--- GET LATEST ROW FOR EACH INSTANCE, TRANSFORM DATE COLUMNS AND INSERT INTO INTERFACE_INSTANCES
create table interface_instances as
select
    correlationID,
    to_timestamp(substr(correlationID, 1, 17), 'yyyymmddhh24missff') start_timestamp,
    to_timestamp(substr(logid, 1, 17), 'yyyymmddhh24missff') end_timestamp,
    message,
    status,
    type,
    custom_1,
    custom_2,
    hostname
from (
    select 
        t.*,
        row_number() over(partition by correlationID order by logid desc) rn
    from logging_events t
) t
where rn = 1;

---- COUNT ROWS IN INTERFACE_INSTANCES
select count(*) from interface_instances;