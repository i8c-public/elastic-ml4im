# ML4IM AWS Virtual Machine  
![alt text](../assets/ml4im_elk.PNG)  
This is the main component of the ML4IM project. It consists out of a AWS virtual machine created docker-machine. 
The VM is used to run containers created by docker compose, and hosts a container for Kibana, ElasticSearch, Oracle XE & Logstash.
  
| Specifications:|                  | Open ports: |      | 
|----------------|------------------|-------------|------|
| OS             | Ubuntu (Default) | Kibana      | 5601 |
| Instance Type  | t2.2xlarge       | Elastic HTTP| 9200 |
| Name           | ML4IMDocker      | Oracle XE   | 1521 |
| Region         | eu-west-1        | HTTP        | 80   |
| Security-group | ML4IMDocker      | HTTPS       | 443  |
|                |                  | SSH         | 22   |  

The t2.2xlarge instance type provides the following hardware:

| t2.2xlarge          |                     |
|---------------------|---------------------|
| vCPUs               | 8                   |
| Architecture        | x86_64 (64-bit)     |
| Memory              | 32768 MiB           |
| Network performance | Moderate            |
| Storage             | 60 GB (EBS)         |
| Pricing (Linux)     | 0.4032 USD per Hour |  
  
  > More information on creating AWS EC2 instances using docker-machine can be found [here.](https://docs.docker.com/machine/examples/aws/)   
   
## Table of Contents
[[_TOC_]]
  

## Requirements
> This installation requires that you already installed the [ML4IM Development Desktop](ml4im_dev_desktop/readme.md) or a machine with the required tools. The AWS VM is hosted by Amazon Web Services, if you wish to create the demonstration setup you must have AWS credentials to setup a virtual machine.  
  
### Hardware
During the installation you will create a new AWS virtual machine. The parameters used during installation will comply to all these requirements:
* At least 32GB working memory (Based on machine learning complexity)
* A processor that supports SSE4.2 instructions
* A 64 bit-system
* EBS storage (This allows you to expand storage space if necessary)

### Software
The required software is already installed using the custom vagrant file with the Development Desktop:
* AWS CLI
* Docker-machine
* Docker

## Installation
> The installation is executed in a bash terminal on the Development Desktop.
#### 1. [Configure AWS with your credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)
```shell
$ aws configure
AWS Access Key ID [None]: [Enter your Access Key ID here]
AWS Secret Access Key [None]: [Enter your Secret Access Key here]
Default region name [None]: [Enter your preferred region - Example: 'eu-west-1']
Default output format [None]: [You can leave this empty]
```
#### 2. Create VM using Docker-machine
```shell
$ docker-machine create --driver amazonec2  --amazonec2-open-port 443  --amazonec2-open-port 80 --amazonec2-open-port 1521 --amazonec2-open-port 9200 --amazonec2-open-port 5601 --amazonec2-instance-type t2.2xlarge --amazonec2-region eu-west-1 --amazonec2-security-group ML4IMDocker --amazonec2-root-size 60 ML4IMDocker
```  
  
When the virtual machine is created, it should show up in the [EC2 Management Instance List](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:sort=instanceId)  
![Instace List Example Image](../assets/instanceListExample.png)  
Make sure that the EC2 instance has been completely setup and status "RUNNING" before continueing.    
**After creating the virtual machine it is *strongly* advised that you create a backup of  `/home/vagrant/.docker`. This contains the certificates required to access the virtual machine.**  
> More information on using the EC2 Console can be found [here.](https://aws.amazon.com/ec2/getting-started/)  
    
#### 3. Execute setup_aws.sh  
The `setup_aws.sh` script provided at `/ml4im_dev_desktop` will prepare the AWS Virtual Machine to run the ML4IM project. This will automate moving files and installing docker-compose. 

```shell
# Executing the script
$ /vagrant/ml4im_dev_desktop/setup_aws.sh
```  
   
<details>
<summary>This summary of setup_aws.sh explains the inner workings of the script.</summary>
  
- **Copy the neccesary files to the AWS VM** 

The following command will copy the `/vagrant/ml4im_docker` directory on your local VM, to `/home/ubuntu/ml4imDocker` on the AWS VM:
```shell
$ docker-machine scp -r /vagrant/ml4im_docker ML4IMDocker:/home/ubuntu/ml4imDocker
```  
This directory contains the docker-compose files to create docker containers for Kibana, Elastic, Logstash & Oracle XE, aswell as any scripts required for preparing the demonstration project.  
  
- **Accessing the AWS VM & install docker-compose**  
```shell
$ docker-machine ssh ML4IMDocker
```
This opens a SSH bash into the virtual machine.   

Docker-compose will be installed using the following commands:
```shell
# Download the latest version of docker-compose (install not required)
$ sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
# Set the permissions so it can be executed
$ sudo chmod +x /usr/local/bin/docker-compose
# Verify the installation
$ docker-compose --version
```  

</details>  

#### 4. Connecting to the AWS Virtual Machine
```shell
$ docker-machine ssh ML4IMDocker
```
This opens a SSH bash into the virtual machine.   
**All commands executed in this terminal in the future are executed on the virtual machine. You can exit using CTRL+D.** 
  
> [See the usage README to launch the docker-containers and start using Elasticsearch's Machine learning](/usage/README.md).  
